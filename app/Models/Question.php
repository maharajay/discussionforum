<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Question extends Model
{
   
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug'; 
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function category()
    {
        return $this->hasMany(Category::class);
    }
}
