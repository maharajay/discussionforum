<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AuthController;

class RegisterController extends Controller
{
    /**
     * @var AuthController 
     */
    public $authController; 

     public function __construct(AuthController $authController)
     {
         $this->authController = $authController;
     }
    public function signup(Request $request){
        User::create($request->all());
        $data = $this->authController->login($request);
        return $data;
    }
}
